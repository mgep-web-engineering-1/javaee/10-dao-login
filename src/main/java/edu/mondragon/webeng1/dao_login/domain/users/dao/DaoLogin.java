package edu.mondragon.webeng1.dao_login.domain.users.dao;

public interface DaoLogin {
    public boolean exists(String username, String password);
}
