# DAO Login

## Make it work

- Clone the repository.
- Open folder with VSCode.
- Right click and Run `src > main > java > ... > Main.java`.
- Open [http://localhost:8080](http://localhost:8080) in the browser.

## Objective

The application will show a form to login.

We want to create an application that enables an easy way to change the database. We will have 2 different ways to check if the entered user exists:

1. Searching in a MySQL database (127.0.0.1:3306).
1. Searching in a properties file (file_database.properties).

Just changing a property in ```src > main > resources > db.properties``` (from ```use=file``` to ```use=mysql```) the application will change where the user is authenticated (needs restarting).

We have different passwords for both cases, so we can see that it works.

**In a real world excenario, a property file wouldn't be the best way to authenticate a user. We would probably have a bunch of different databases (MySQL, SQLite, PostgreSQL...). Many applications do something similar (e.g. Drupal, wordpress, Mediawiki...). As I don't want you to install and configure multiple databases, I hope a property file would make the point.**

## Explaination

We will explain some code and patterns to understand this application.

![Class diagram](./uml/class_diagram.png "Class diagram")

### Model View Controller pattern (MVC)

So we have 3 parts:

1. **Model**: where the business logic is. If our application transforms from a *Web application* to a *desktop application* or a *Comman Line Interface application* in the future, this part should be able to maintain unaltered. Most of Java files are inside this category.
1. **View**: this is how the interface will be printed. In this case, JSP files will be the view.
1. **Controller**: This will get the request from the view, will call the model (business logic), and will redirect/dispatch the needed response. In Java, Servlets are the Controllers.

So, to summarize, **views** will be inside ```src > main > java > webapp``` folder, **controllers** will be inside ```src > main > java > ... > controller``` and the rest of files inside ```src > main > java > ...``` folder will be the **model**.

### Data Access Object pattern (DAO)

[DAO pattern](https://www.baeldung.com/java-dao-pattern)'s objective is to **isolate application/bussiness layer from the peristence layer**. This is, out application will work with objects, and DAO classes will do the transformation of those objects to database records (using SELECT, INSERT, DELETE, UPDATE...).

This will be better explained in the next exercise, but it is a way of accessing database table records as java objects.

We will have a ```DaoLogin``` interface, that will list the function(s) all the different implementations of the interface should implement.

For example, this DaoLogin interface has one single function:

```java
public boolean exists(String username, String password);
```

We will have one inplementation of this interface per database. In this case, ```DaoLoginMySQL``` and ```DaoLoginProperties``` will implement ```DaoLogin``` an its ```exists``` funciton and see if there is a user with the given username and password.

### Singleton pattern

[Singleton pattern](https://refactoring.guru/design-patterns/singleton) ensures that a class has just one instance.

In this code ```MySQLConfig.java``` file is a singleton instance. The first time it is created, mysql configuration is loaded from ```db.properties``` file(MySQL username, password).

If the class were not Singleton, the ```db.properties``` file would be loaded every time we instantiate the class.

Being singleton, the file is loaded just once, and the instance of the class will be kept alive.

### Facade pattern

[Facade](https://refactoring.guru/design-patterns/facade) allows simplfying complex subsystems in a single class. That way, we present a simple interface (in this case to the controllers) to access different use cases of our application.

This is the only class that will know that there are multiple implementations of  ```DaoLogin```. The rest of the class will just call ```LoginFacade``` and this class will call the implemntation.

This ```LoginFacade``` has a variable with the interface:

```java
private DaoLogin login = null;
```

The constructor determines which implementation will be used.

```java
String databaseToUse = loadDatabaseToUseFromFile(); // Get which Database is selected in database.properties

if(databaseToUse.equals("mysql")) {
    login = new DaoLoginMySQL();
}else {
    login = new DaoLoginProperties();
}
```

The other function will call the function of the implementation.

```java
public boolean exists(String username, String password){
    return login.exists(username, password);
}
```

**It may see weird to have 3 different places to set this ```exists``` functions (Facade+Dao+Implementations), but is very usefull to separate the Database access and the rest of the business logic. Moreover, we will see in future applicaitons, not all the DAO functions have to be implemented in the Facade class.**

### index.jsp (Login Form)

This application has a single view: ```index.jsp```. Depending of the session, it will show a form to login or a wellcome message usign JSTL ```<c:choose>``` tag.

The login form will send 3 parameters:

1. Username.
1. Password.
1. Action (for the controller to know if we are trying to sign in or sign out).

### LoginController

The controller does 2 things, open session or close sessión.

```java
// LoginController.java
...
switch (action) {
    case "login" -> login(session, username, password);
    default -> logout(session);
}

response.sendRedirect("index.jsp");
```

Login will call the facade (never the DAO):

```java
LoginFacade lf = new LoginFacade();
exists = lf.exists(username, password);
System.out.println("Exists:" + exists);
```

Then it will add a message:

```java
if (exists) {
  session.setAttribute("username", username);
  session.removeAttribute("wrongUsername");
  session.setAttribute("message", "Successfully loged!");
} else {
  session.setAttribute("wrongUsername", username);
  session.removeAttribute("username");
  session.setAttribute("message", "Successfully loged!");
}
```

Logut will remove modify session to store that the user is not loged anymore:

```java
session.removeAttribute("username");
session.removeAttribute("wrongUsername");
session.setAttribute("message", "You loged out.");
```

### index.jsp (again)

Now we can understand what is happening here. If **username** is in the session, then wellcome message and a logut form will show, otherwise, the login form.

```jsp
<c:choose>
  <c:when test="${not empty sessionScope.username}">
    ... <!-- Wellcome message and logout form -->
  </c:when>
  <c:otherwise>
    ... <!-- Login messge -->
  </c:otherwise>
</c:choose>
```

We can see something similar with success & error messages. If there is any, it will be shown and deleted from the session:

```jsp
<!-- Messages & errors -->
<c:if test="${not empty sessionScope.error}">
  <p class="error"><c:out value="${sessionScope.error}" /></p>
  <c:remove var="error" scope="session" />
</c:if>
<c:if test="${not empty sessionScope.message}">
  <p class="message"><c:out value="${sessionScope.message}" /></p>
  <c:remove var="message" scope="session" />
</c:if>
```

## Check that it works

Using properties file:

1. Check that ```db.properties``` has ```use=file``` set.
1. Run Main.java.
1. Open [http://localhost:8080](http://localhost:8080) in the web browser.
1. **admin** as username, **admin@prop** as password.
1. A success message should show, *Hi admin!* and also a logout form.
1. Click on *Logout*.
1. A success *Logout* message should show and also the login form again.
1. Stop application (```CTRL + C``` VSCode terminal).

Using MySQL 8 database:

1. MySQL has to be configured with the same parameters as in ```db.poperties``` file:
    - Create ```mvc_exercise``` database in MySQL.
    - Execute ```mvc_exercise.sql``` commands in MySQL:
        - It creates a MySQL user named **admin**.
        - It creates a ```user``` table for the application.
        - It creates a ```news_item``` table for MVC Exercise 3.
        - Fills the table with some values.
    - MySQL shoud be at the default port **3306** and in localhost (**127.0.0.1**).
    - **Remember, it is not the same MySQL user and our application user!**
      - MySQL user is used to create the connection with the database.
      - Application user is used to login in our application.
    - **This configuration will be used to correct the exam, so do not change it**.
    - It will also be used in the next exercises.

1. Check that ```db.properties``` has ```use=mysql``` set, otherwise change it
1. Run Main.java.
1. Open [http://localhost:8080](http://localhost:8080) in the web browser.
1. **admin** as username, **admin@eskola** as password.
1. A success message should show, *Hi admin!* and also a logout form.
1. Click on *Logout*.
1. A success *Logout* message should show and also the login form again.
1. Stop application (```CTRL + C``` VSCode terminal).

You can also try any other username and password and it should not login. Even using mysql password with file configuration and vice versa.

## Next and before

- Before: [09-internationalizaiton](https://gitlab.com/mgep-web-engineering-1/javaee/09-internationalization)

- Next: [11-dao-user](https://gitlab.com/mgep-web-engineering-1/javaee/11-dao-user)
