<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="css/style.css" />
    <title>DAO login</title>
  </head>
  <body>
    <!-- Messages & errors -->
    <c:if test="${not empty sessionScope.error}">
      <p class="error"><c:out value="${sessionScope.error}" /></p>
      <c:remove var="error" scope="session" />
    </c:if>
    <c:if test="${not empty sessionScope.message}">
      <p class="message"><c:out value="${sessionScope.message}" /></p>
      <c:remove var="message" scope="session" />
    </c:if>

    <!-- Login form or Greetings -->
    <c:choose>
      <c:when test="${not empty sessionScope.username}">
        <!-- print username -->
        <h1>Hi <c:out value="${sessionScope.username}" />!</h1>
        <p>You are loged!</p>
        <!-- logout form -->
        <form action="/login">
          <button type="submit" name="action" value="logout">
            Logout
          </button>
        </form>
      </c:when>
      <c:otherwise>
        <h1>Login</h1>
        <form action="/login" method="post">
          <!-- login form -->
          <label>
            Username:
            <input
              type="text"
              name="username"
              required
              value="<c:out value='${sessionScope.wrongUsername}' default='' />"
              placeholder="Username"
            />
            <c:remove var="wrongUsername" scope="session" />
          </label>
          <br />
          <label>
            Password:
            <input
              type="password"
              name="password"
              required
              placeholder="Password"
            />
          </label>
          <br />
          <button type="submit" name="action" value="login">Login</button>
        </form>
      </c:otherwise>
    </c:choose>
  </body>
</html>
