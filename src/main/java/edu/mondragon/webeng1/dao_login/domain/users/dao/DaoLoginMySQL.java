package edu.mondragon.webeng1.dao_login.domain.users.dao;

import edu.mondragon.webeng1.dao_login.config.MySQLConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DaoLoginMySQL implements DaoLogin {
    private MySQLConfig mysqlConfig;

    public DaoLoginMySQL() {
        mysqlConfig = MySQLConfig.getInstance();
    }

    @Override
    public boolean exists(String username, String password) {
        boolean exists = false;

        String sqlQuery = "SELECT * FROM user WHERE username=? AND password=?";
        Connection connection = mysqlConfig.connect();
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(sqlQuery);
            stm.setString(1, username);
            stm.setString(2, password);
            System.out.println(stm);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                exists = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error DaoLoginMysql exists");
        }
        mysqlConfig.disconnect(connection, stm);
        return exists;
    }

}
