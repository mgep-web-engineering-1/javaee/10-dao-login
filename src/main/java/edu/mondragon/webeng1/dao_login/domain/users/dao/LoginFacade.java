package edu.mondragon.webeng1.dao_login.domain.users.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class LoginFacade {
    private DaoLogin login = null;

    public LoginFacade() {
        String databaseToUse = loadDatabaseToUseFromFile();

        if (databaseToUse.equals("mysql")) {
            login = new DaoLoginMySQL();
        } else {
            login = new DaoLoginProperties();
        }

    }

    public boolean exists(String username, String password) {
        return login.exists(username, password);
    }

    private String loadDatabaseToUseFromFile() {
        String databaseToUse = "file"; // default value
        Properties prop = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            prop.load(classLoader.getResourceAsStream("db.properties"));
            databaseToUse = prop.getProperty("use");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return databaseToUse;
    }
}
