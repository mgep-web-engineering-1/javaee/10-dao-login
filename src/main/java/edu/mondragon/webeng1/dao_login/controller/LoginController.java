package edu.mondragon.webeng1.dao_login.controller;

import java.io.IOException;
import java.util.Optional;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import edu.mondragon.webeng1.dao_login.domain.users.dao.LoginFacade;

@WebServlet(name = "LoginController", urlPatterns = { "/login" })
public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public LoginController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Get parameters from the request
        String action = Optional.ofNullable(request.getParameter("action")).orElse("logout");
        String username = Optional.ofNullable(request.getParameter("username")).orElse("");
        String password = Optional.ofNullable(request.getParameter("password")).orElse("");
        HttpSession session = request.getSession(true);

        System.out.println("Username: " + username);
        System.out.println("Password: " + password
                + " (This should never be done in real projects! Printing passwords in logs is a bad practice.)");
        
        switch (action) {
            case "login" -> login(session, username, password);
            default -> logout(session);
        }

        response.sendRedirect("index.jsp");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    private void login(HttpSession session, String username, String password) {
        boolean exists = false;

        // Check if the user exists in the database/properties file
        LoginFacade lf = new LoginFacade();
        exists = lf.exists(username, password);
        System.out.println("Exists:" + exists);

        // Save login result in session
        if (exists) {
            session.setAttribute("username", username);
            session.removeAttribute("wrongUsername");
            session.setAttribute("message", "Successfully loged!");
        } else {
            session.setAttribute("wrongUsername", username);
            session.removeAttribute("username");
            session.setAttribute("error", "Wrong username or password!");
        }
    }

    private void logout(HttpSession session) {
        session.removeAttribute("wrongUsername");
        session.removeAttribute("username");
        session.setAttribute("message", "You loged out.");
    }

}
